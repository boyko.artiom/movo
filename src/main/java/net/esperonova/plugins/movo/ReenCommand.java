package net.esperonova.plugins.movo;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ReenCommand implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length != 0) return false;

		if(sender instanceof Player) {
			Player player = (Player) sender;

			Location location = MovoPlugin.backLocations.get(player.getUniqueId());
			if(location == null) {
				sender.sendMessage("Vi ne havas reen-lokon");
			}
			else {
				MovoPlugin.backLocations.put(player.getUniqueId(), player.getLocation());
				player.teleport(location);
			}
		}
		else {
			sender.sendMessage("Nur ludantoj povas uzi ĉi tiun");
		}

		return true;
	}
}
