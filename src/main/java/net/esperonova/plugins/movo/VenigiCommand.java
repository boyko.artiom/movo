package net.esperonova.plugins.movo;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.UUID;

public class VenigiCommand implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length != 1) return false;

		if(sender instanceof Player) {
			Player player = (Player) sender;
			Player target = player.getServer().getPlayer(args[0]);
			if(target == null) {
				sender.sendMessage("Ne trovis tiun ludanton");
			}
			else if(player.getUniqueId().equals(target.getUniqueId())) {
				sender.sendMessage("Vi ne povas teleporti al vi mem!");
			}
			else {
				if(MovoPlugin.aliriRequests.containsKey(target.getUniqueId()) && MovoPlugin.aliriRequests.get(target.getUniqueId()).remove(player.getUniqueId())) {
					MovoPlugin.backLocations.put(player.getUniqueId(), player.getLocation());
					target.teleport(player.getLocation());
				}
				else {
					MovoPlugin.venigiRequests.computeIfAbsent(player.getUniqueId(), (_key) -> new HashSet<>()).add(target.getUniqueId());

					target.sendMessage(player.getName() + " volas vin teleporti al ri. Por akcepti, tajpu /aliri " + player.getName());
					player.sendMessage("Sendis teleporto-peton.");
				}
			}
		}
		else {
			sender.sendMessage("Nur ludantoj povas uzi ĉi tiun");
		}

		return true;
	}
}
