package net.esperonova.plugins.movo;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.UUID;

public class AliriCommand implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(args.length != 1) return false;

		if(sender instanceof Player) {
			Player player = (Player) sender;
			Player target = player.getServer().getPlayer(args[0]);
			if(target == null) {
				sender.sendMessage("Ne trovis tiun ludanton");
			}
			else if(player.getUniqueId().equals(target.getUniqueId())) {
				sender.sendMessage("Vi ne povas teleporti al vi mem!");
			}
			else {
				if(MovoPlugin.venigiRequests.containsKey(target.getUniqueId()) && MovoPlugin.venigiRequests.get(target.getUniqueId()).remove(player.getUniqueId())) {
					MovoPlugin.backLocations.put(player.getUniqueId(), player.getLocation());
					player.teleport(target.getLocation());
				}
				else {
					MovoPlugin.aliriRequests.computeIfAbsent(player.getUniqueId(), (_key) -> new HashSet<>()).add(target.getUniqueId());

					target.sendMessage(player.getName() + " volas teleporti al vi. Por akcepti, tajpu /venigi " + player.getName());
					player.sendMessage("Sendis teleporto-peton.");
				}
			}
		}
		else {
			sender.sendMessage("Nur ludantoj povas uzi ĉi tiun");
		}

		return true;
	}
}
